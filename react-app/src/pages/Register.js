import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){
	const navigate = useNavigate()

	const { user } = useContext(UserContext)

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email 
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: "User Already exists!",
					icon: "error",
					text: "The email you provided already exists in the server! Provide another email for registration."
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					if(typeof result !== "undefined"){
						// For clearing out the form
						setFirstName('')
						setLastName('')
						setMobileNumber('')
						setEmail('')
						setPassword1('')
						setPassword2('')

						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login")
					}
				}).catch(error => {
					Swal.fire({
						title: "Something went wrong :(",
						icon: "error",
						text: error.message
					})
				})
			}
		})
	}

	// For form validation, we use the 'useEffect' to track the values of the input fields and run a validation condition everytime there is user input in those fields.
	useEffect(() => {
		// All input fields must not be empty and the password/verify passwords fields must match in values.
		if((firstName !== '' && lastName !== '' && mobileNumber !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])

	// MINI ACTIVITY (20 mins.) 7:55PM
	// Conditionally render the register form to check first if the user is logged in. If so, then redirect to the Courses page, but if not then show the register form.
	// Once finished, push to gitlab and link to boodle.
	return(
		(user.id !== null) ?
			<Navigate to="/courses"/>
		: 
		<Row>
			<Col>
				<Form onSubmit={(event) => registerUser(event)}>
					<h1>Register</h1>
					<Form.Group controlId="userFirstName">
			            <Form.Label>First Name</Form.Label>
			            <Form.Control 
			                type="text" 
			                placeholder="Enter your First Name" 
			                value={firstName}
			                onChange={event => setFirstName(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="userLastName">
			            <Form.Label>Last Name</Form.Label>
			            <Form.Control 
			                type="text" 
			                placeholder="Enter your Last Name" 
			                value={lastName}
			                onChange={event => setLastName(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="userMobileNumber">
			            <Form.Label>Mobile Number</Form.Label>
			            <Form.Control 
			                type="text" 
			                placeholder="Enter your Mobile Number" 
			                value={mobileNumber}
			                onChange={event => setMobileNumber(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="userEmail">
			            <Form.Label>Email address</Form.Label>
			        {/* 2-way Data Binding is when the value of the state reflects on the value of the input field, and vice-versa. The way to implement it is by using an 'onChange' event listener on the input field and updating the value of the state to the current value of the input field. */}
			            <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value={email}
			                onChange={event => setEmail(event.target.value)}
			                required
			            />
			            <Form.Text className="text-muted">
			                We'll never share your email with anyone else.
			            </Form.Text>
			        </Form.Group>

			        <Form.Group controlId="password1">
			            <Form.Label>Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value={password1}
			                onChange={event => setPassword1(event.target.value)} 
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="password2">
			            <Form.Label>Verify Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                value={password2}
			                onChange={event => setPassword2(event.target.value)}
			                required
			            />
			        </Form.Group>

			    	{/* Conditional Rendering is when you render elements depending on a specific logical condition */}
			        { isActive ?
			        	<Button variant="primary" type="submit" id="submitBtn">
				        	Submit
				        </Button>
				      	:
				      	<Button disabled variant="primary" type="submit" id="submitBtn">
				        	Submit
				        </Button>
			        }
			    </Form>
			</Col>
		</Row>
	)
}