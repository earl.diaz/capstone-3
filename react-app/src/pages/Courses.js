import {useState, useEffect, Fragment} from 'react'
import CourseCard from '../components/CourseCard'
import LoadingSpinner from '../components/LoadingSpinner'
import { Row, Col } from 'react-bootstrap'

export default function Courses(){
	const [courses, setCourses] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	// Will run upon the initial rendering of the 'Courses' component since there are no values on the 2nd argument which is the array.
	useEffect(() => {
		// Sets the isLoading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(result => {
			// Sets the isLoading state to false
			setIsLoading(false)

			setCourses(result.map(course => {
				return (
					<CourseCard key={course._id} course={course}/>
				)
			}))
		})
	}, [])

	return(
		<Row>
			<Col>
				{/*Using the 'courses' variable which will return the components to be rendered based on the data that was looped.*/}
				{/* Checks the value of isLoading state to determine wether to render the spinner or the actual data itself.*/}
				{ isLoading ?
						<LoadingSpinner/>
					:
				 		courses 
				}
			</Col>
		</Row>
	)
}